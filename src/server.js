const express = require("express");
const app = express();
const port = process.env.PORT || 3333;

//rotas
const routes = require('./routes');

//apply middleware default
app.use(express.json());
app.use(routes);

//rodando o servidor
app.listen(port, () => console.log(`## servidor rodando na porta ${port}`));