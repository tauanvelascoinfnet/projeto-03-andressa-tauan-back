const Sequelize = require("sequelize"); // importando o Sequelize
const dbConfig = require("../config/database"); // importando do arquivo de config

const connection = new Sequelize(dbConfig.development); // passando para a conexão os dados informados no dbConfig

module.exports = connection;