const { Router } = require("express");
const express = require("express");
const PacoteController = require("./controller/PacoteController");

const routes = express.Router();

routes.get('/pacotes', PacoteController.index)

module.exports = routes;