module.exports = {
  index(req, res) {
    const pacotes = [
      {
        //id: 1,
        nome: "Nova York",
        descricao: "Conheça Nova York por apenas R$1000 com o Cristiano!",
        valor: 1000.00,
        url_imagem: "https://ci-prod.imgix.net/upload/galeriadeimagens/00205891.jpg",
        data_ida: "09-09-2021",
        data_volta: "16-09-2021",
        duracao: 7,
        inclui_hotel: true,
        inclui_passagem: true,
        inclui_transfer: true,
        //inscricoes: [],
      },
      {
        //id: 2,
        nome: "Rio de Janeiro",
        descricao: "Visite os melhores pontos turísticos do Rio!",
        valor: 6000.00,
        url_imagem: "https://media.tacdn.com/media/attractions-splice-spp-674x446/06/6f/5f/fa.jpg",
        data_ida: "09-09-2021",
        data_volta: "16-09-2021",
        duracao: 7,
        inclui_hotel: true,
        inclui_passagem: true,
        inclui_transfer: true,
        //inscricoes: []
      },
      {
        //id: 3,
        nome: "Tokyo",
        descricao: "Conheça o melhor do Japão! Sem terremotos!",
        valor: 850.00,
        url_imagem: "https://coris.com.br/blog/wp-content/uploads/2019/06/18-06-19-.jpg",
        data_ida: "09-09-2021",
        data_volta: "16-09-2021",
        duracao: 7,
        inclui_hotel: true,
        inclui_passagem: true,
        inclui_transfer: true,
        //inscricoes: []
      },
      {
        //id: 4,
        nome: "Paris",
        descricao: "Visite o cartão postal de Paris! E os melhores museus!",
        valor: 2560.00,
        url_imagem: "https://viagemeturismo.abril.com.br/wp-content/uploads/2016/11/thinkstockphotos-4549879531.jpeg?quality=70&strip=info&w=680&h=453&crop=1",
        data_ida: "09-09-2021",
        data_volta: "16-09-2021",
        duracao: 7,
        inclui_hotel: true,
        inclui_passagem: true,
        inclui_transfer: true,
        //inscricoes: []
      },
    ];

    return res.json(pacotes);
  },
};
